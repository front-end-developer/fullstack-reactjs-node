{
  parserOptions: {
        ecmaVersion: 6,
        ecmaFeatures: {
          experimentalObjectRestSpread: true,
          jsx: true
        },
        sourceType: "module"
    },
    env: {
        es6: true,
        node: true,
        jquery: true,
        mocha: true
    },
    plugins: [
        "react",
        "import",
        "node",
        "promise"
    ],
    globals: {
    document: false,
        navigator: false,
        window: false
    },
    rules: {
        accessor-pairs: "error",
        arrow-spacing: [
            "error",
            {
                before: true,
                after: true
            }
        ],
        block-spacing: [
            "error",
            "always"
        ],
        brace-style: [
            "error",
            "1tbs",
            {
                allowSingleLine: true
            }
        ],
            camelcase: [
            "error",
            {
                properties: "never"
            }
        ],
        comma-dangle: [
            "error",
            {
                arrays: "never",
                objects: "never",
                imports: "never",
                exports: "never",
                functions: "never"
            }
        ],
        comma-spacing: [
            "error",
            {
                before: false,
                after: true
            }
        ],
        comma-style: [
            "error",
            "last"
        ],
        constructor-super: "error",
            curly: [
            "error",
            "multi-line"
        ],
        dot-location: [
            "error",
            "property"
        ],
        func-call-spacing: [
            "error",
            "never"
        ],
        generator-star-spacing: [
            "error",
            {
                before: true,
                after: true
            }
        ],
        handle-callback-err: [
            "error",
            "^(err|error)$"
        ],
        key-spacing: [
            "error",
            {
                beforeColon: false,
                afterColon: true
            }
        ],
        keyword-spacing: [
            "error",
            {
                before: true,
                after: true
            }
        ],
        new-cap: [
            "error",
            {
                newIsCap: true,
                capIsNew: false
            }
        ],
        no-delete-var: "error",
        no-dupe-args: "error",
        no-dupe-class-members: "error",
        no-dupe-keys: "error",
        no-duplicate-case: "error",
        no-empty-character-class: "error",
        no-empty-pattern: "error",
        no-eval: "error",
        no-ex-assign: "error",
        no-extend-native: "error",
        no-extra-bind: "error",
        no-extra-boolean-cast: "error",
        no-extra-parens: [
            "error",
            "functions"
        ],
        no-fallthrough: "error",
        no-floating-decimal: "error",
        no-func-assign: "error",
        no-global-assign: "error",
        no-implied-eval: "error",
        no-inner-declarations: [
            "error",
            "functions"
        ],
        no-invalid-regexp: "error",
        no-irregular-whitespace: "error",
        no-iterator: "error",
        no-label-var: "error",
        no-labels: [
            "error",
            {
                allowLoop: false,
                allowSwitch: false
            }
        ],
        no-lone-blocks: "error",
        no-multiple-empty-lines: [
            "error",
            {
                max: 3,
                maxEOF: 3
            }
        ],
        import/export: "error",
        import/first: "error",
        import/no-duplicates: "error",
        import/no-webpack-loader-syntax: "error",

        react/prop-types: 1,
        react/prefer-es6-class: 1,
        react/jsx-uses-vars: 1,
        react/jsx-pascal-case: 1
    }
}