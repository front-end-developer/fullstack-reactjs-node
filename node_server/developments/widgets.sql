/*
 Navicat PostgreSQL Data Transfer

 Source Server         : localhost_5432
 Source Server Type    : PostgreSQL
 Source Server Version : 100004
 Source Host           : localhost:5432
 Source Catalog        : appijuiceDev
 Source Schema         : appijuiceTest

 Target Server Type    : PostgreSQL
 Target Server Version : 100004
 File Encoding         : 65001

 Date: 12/07/2018 19:14:57
*/


-- ----------------------------
-- Table structure for widgets
-- ----------------------------
DROP TABLE IF EXISTS "appijuiceTest"."widgets";
CREATE TABLE "appijuiceTest"."widgets" (
  "uuid" uuid NOT NULL,
  "user_uuid" uuid NOT NULL,
  "widgets" json,
  "active" varchar(25) COLLATE "pg_catalog"."default" NOT NULL,
  "createdBy" varchar(254) COLLATE "pg_catalog"."default" NOT NULL,
  "modifiedBy" varchar(254) COLLATE "pg_catalog"."default" NOT NULL,
  "dateCreated" timestamptz(6) NOT NULL,
  "dateModified" timestamptz(6) NOT NULL
);

-- ----------------------------
-- CUST AND PAST AND RUN INSERTS ONE BY ONE,  OTHERWISE YOU WILL GET INSERT ERRORS,
-- ITS NOT LIKE MYSQL:). 
-- Records of widgets
-- ----------------------------

INSERT INTO "appijuiceTest".widgets VALUES (
'ffb11fdc-29af-402e-8885-17f337201abd',
'ddb11fdc-29af-402e-8885-17f337201abd',
'{
	"search": {
		"keyword": ""
	},
	  "tasks": [{
		"name": "Angular",
		"category": "widgetInventory",
		"bgcolor": "yellow"
	}, {
		"name": "React",
		"category": "widgetInventory",
		"bgcolor": "pink"
	}, {
		"name": "Material",
		"category": "widgetInventory",
		"bgcolor": "yellow"
	}, {
		"name": "redux",
		"category": "widgetInventory",
		"bgcolor": "pink"
	}, {
		"name": "Vue",
		"category": "dashboard",
		"bgcolor": "skyblue"
	}, {
		"name": "java",
		"category": "dashboard",
		"bgcolor": "skyblue"
	}]
}',
'active',
'mark',
'mark',
now(),
now()
)

INSERT INTO "appijuiceTest".widgets VALUES (
'22b11fdc-29af-402e-8885-17f337201abd',
'bbb11fdc-29af-402e-8885-17f337201abd',
'{
	"search": {
		"keyword": ""
	},
	  "tasks": [{
		"name": "Angular",
		"category": "widgetInventory",
		"bgcolor": "yellow"
	}, {
		"name": "React",
		"category": "widgetInventory",
		"bgcolor": "pink"
	}, {
		"name": "Material",
		"category": "widgetInventory",
		"bgcolor": "yellow"
	}, {
		"name": "redux",
		"category": "widgetInventory",
		"bgcolor": "pink"
	}, {
		"name": "Vue",
		"category": "dashboard",
		"bgcolor": "skyblue"
	}, {
		"name": "java",
		"category": "dashboard",
		"bgcolor": "skyblue"
	}]
}',
'active',
'webley',
'webley',
now(),
now()
)