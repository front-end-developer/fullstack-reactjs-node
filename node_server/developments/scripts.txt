INSERT INTO "appijuiceTest".users VALUES (
'ddb11fdc-29af-402e-8885-17f337201abd',
'mark',
'$2a$10$ipCiMnqYzfcacpfXfBEp8.9evXm9yrxswgZocaCJ94xQxfhki5tnK', 
 'ADMIN',
	'active','seed',
	'seed',
	'now()','now()','mrkwebley@gmail.com'
)

INSERT INTO "appijuiceTest".users VALUES (
'bbb11fdc-29af-402e-8885-17f337201abd',
'webley',
'$2a$10$ipCiMnqYzfcacpfXfBEp8.9evXm9yrxswgZocaCJ94xQxfhki5tnK', 
 'ADMIN',
	'active','seed',
	'seed',
	'now()','now()','markw@adobeconsultant.co.uk'
)

Widget Scripts:
INSERT INTO "appijuiceTest".widgets VALUES (
'ffb11fdc-29af-402e-8885-17f337201abd',
'ddb11fdc-29af-402e-8885-17f337201abd',
'{search:{keyword:''},tasks:[{name: "Angular",category: "widgetInventory",bgcolor: "yellow"},{name: "React",category: "widgetInventory",bgcolor: "pink"},{name: "Material",category: "widgetInventory",bgcolor: "yellow"},{name: "redux",category: "widgetInventory",bgcolor: "pink"},{name: "Vue",category: "dashboard",bgcolor: "skyblue"},{name: "java",category: "dashboard",bgcolor: "skyblue"}]}}',
'active',
'mark',
'mark',
'now()',
'now()'
)


INSERT INTO "appijuiceTest".widgets VALUES (
'22b11fdc-29af-402e-8885-17f337201abd',
'bbb11fdc-29af-402e-8885-17f337201abd',
'{search:{keyword:''},tasks:[{name: "Angular",category: "widgetInventory",bgcolor: "yellow"},{name: "React",category: "widgetInventory",bgcolor: "pink"},{name: "Material",category: "widgetInventory",bgcolor: "yellow"},{name: "redux",category: "widgetInventory",bgcolor: "pink"},{name: "Vue",category: "dashboard",bgcolor: "skyblue"},{name: "java",category: "dashboard",bgcolor: "skyblue"}]}}',
'active',
'webley',
'webley',
now(),
now()
)

delete scripts:
delete from "appijuiceTest".widgets where uuid = 'bbb11fdc-29af-402e-8885-17f337201abd'

