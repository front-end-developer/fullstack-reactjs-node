/*
 Navicat PostgreSQL Data Transfer

 Source Server         : localhost_5432
 Source Server Type    : PostgreSQL
 Source Server Version : 100004
 Source Host           : localhost:5432
 Source Catalog        : appijuiceDev
 Source Schema         : appijuiceTest

 Target Server Type    : PostgreSQL
 Target Server Version : 100004
 File Encoding         : 65001

 Date: 12/07/2018 20:26:15
*/


-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "appijuiceTest"."users";
CREATE TABLE "appijuiceTest"."users" (
  "uuid" uuid NOT NULL,
  "login" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "type" varchar(100) COLLATE "pg_catalog"."default",
  "active" varchar(25) COLLATE "pg_catalog"."default" NOT NULL,
  "createdBy" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "modifiedBy" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "dateCreated" timestamptz(6) NOT NULL,
  "dateModified" timestamptz(6) NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "appijuiceTest"."users" VALUES ('ddb11fdc-29af-402e-8885-17f337201abd', 'mark', '$2a$10$ipCiMnqYzfcacpfXfBEp8.9evXm9yrxswgZocaCJ94xQxfhki5tnK', 'ADMIN', 'active', 'seed', 'seed', '2018-07-05 21:29:03.911633+01', '2018-07-05 21:29:03.911633+01', 'mrkwebley@gmail.com');
INSERT INTO "appijuiceTest"."users" VALUES ('bbb11fdc-29af-402e-8885-17f337201abd', 'mark', '$2a$10$ipCiMnqYzfcacpfXfBEp8.9evXm9yrxswgZocaCJ94xQxfhki5tnK', 'ADMIN', 'active', 'seed', 'seed', '2018-07-05 21:30:14.245087+01', '2018-07-05 21:30:14.245087+01', 'markw@adobeconsultant.co.uk');
