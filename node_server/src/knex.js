const Knex = require('knex');
const connection = require('./knex-' + process.env.NODE_ENV);
module.exports = Knex(connection);