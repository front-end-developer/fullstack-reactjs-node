exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('widgets').del()
    .then(function () {
      // Inserts seed entries
      return knex('widgets').insert([
        {
          uuid: "14a400a4-071b-4924-873a-46843f34a7f3", 
		  user_uuid: "ddb11fdc-29af-402e-8885-17f337201abd",
          widgets: "{"search":{"keyword":""},"tasks":[{"name":"Angular","category":"widgetInventory","bgcolor":"yellow"},{"name":"React","category":"widgetInventory","bgcolor":"pink"},{"name":"Material","category":"widgetInventory","bgcolor":"yellow"},{"name":"redux","category":"widgetInventory","bgcolor":"pink"},{"name":"Vue","category":"dashboard","bgcolor":"skyblue"},{"name":"java","category":"dashboard","bgcolor":"skyblue"}]}",
          active: 'active',
          createdBy: 'seed',
          modifiedBy: 'seed',
          dateCreated: 'now()',
          dateModified: 'now()'
        },
        {
          uuid: "ec9395b6-2e7d-4fd3-8a9b-42411bd81515", 
		  user_uuid: "bbb11fdc-29af-402e-8885-17f337201abd",
          widgets: "{"search":{"keyword":""},"tasks":[{"name":"Angular","category":"widgetInventory","bgcolor":"yellow"},{"name":"React","category":"widgetInventory","bgcolor":"pink"},{"name":"Material","category":"widgetInventory","bgcolor":"yellow"},{"name":"redux","category":"widgetInventory","bgcolor":"pink"},{"name":"Vue","category":"dashboard","bgcolor":"skyblue"},{"name":"java","category":"dashboard","bgcolor":"skyblue"}]}",
          active: 'active',
          createdBy: 'seed',
          modifiedBy: 'seed',
          dateCreated: 'now()',
          dateModified: 'now()'
        }                 
      ]);
    });
};