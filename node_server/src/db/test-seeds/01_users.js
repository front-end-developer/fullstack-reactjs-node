exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('FND_USERS').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([{
          uuid: "ddb11fdc-29af-402e-8885-17f337201abd",
          login: 'mark',
          password: '$2a$10$ipCiMnqYzfcacpfXfBEp8.9evXm9yrxswgZocaCJ94xQxfhki5tnK', //1234
          email: 'mrkwebley@gmail.com',
          type: 'ADMIN',
          active: 'active',
          createdBy: 'seed',
          modifiedBy: 'seed',
          dateCreated: 'now()',
          dateModified: 'now()'
        }, 
		{
          uuid: "bbb11fdc-29af-402e-8885-17f337201abd",
          login: 'webley',
          password: '$2a$10$ipCiMnqYzfcacpfXfBEp8.9evXm9yrxswgZocaCJ94xQxfhki5tnK', //1234
          email: 'markw@adobeconsultant.co.uk',
          type: 'ADMIN',
          active: 'active',
          createdBy: 'seed',
          modifiedBy: 'seed',
          dateCreated: 'now()',
          dateModified: 'now()'
        }
      ]);
    });
};