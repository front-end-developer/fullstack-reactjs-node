exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('widgets').del()
    .then(function () {
      // Inserts seed entries
      return knex('widgets').insert([
        {
          uuid: "ffb11fdc-29af-402e-8885-17f337201abd", 
		  user_uuid: "ddb11fdc-29af-402e-8885-17f337201abd",
          widgets: '"{"search":{"keyword":""},"tasks":[{"name":"Angular","category":"widgetInventory","bgcolor":"yellow"},{"name":"React","category":"widgetInventory","bgcolor":"pink"},{"name":"Material","category":"widgetInventory","bgcolor":"yellow"},{"name":"redux","category":"widgetInventory","bgcolor":"pink"},{"name":"Vue","category":"dashboard","bgcolor":"skyblue"},{"name":"java","category":"dashboard","bgcolor":"skyblue"}]}"',
          active: 'active',
          createdBy: 'seed',
          modifiedBy: 'seed',
          dateCreated: 'now()',
          dateModified: 'now()'
        },
        {
          uuid: "22b11fdc-29af-402e-8885-17f337201abd", 
		  user_uuid: "bbb11fdc-29af-402e-8885-17f337201abd",
          widgets: '"{"search":{"keyword":""},"tasks":[{"name":"Angular","category":"widgetInventory","bgcolor":"yellow"},{"name":"React","category":"widgetInventory","bgcolor":"pink"},{"name":"Material","category":"widgetInventory","bgcolor":"yellow"},{"name":"redux","category":"widgetInventory","bgcolor":"pink"},{"name":"Vue","category":"dashboard","bgcolor":"skyblue"},{"name":"java","category":"dashboard","bgcolor":"skyblue"}]}"',
          active: 'active',
          createdBy: 'seed',
          modifiedBy: 'seed',
          dateCreated: 'now()',
          dateModified: 'now()'
        }                 
      ]);
    });
};