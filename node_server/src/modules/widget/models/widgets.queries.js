const Knex = require('../../../knex');
const getWidgets = async (uuid) => {
  try {
    const getAllWidgets = await Knex('appijuiceTest.widgets')
      .select('*')
      .where({
        user_uuid: uuid
      });
    console.log(getAllWidgets[0]);
    return getAllWidgets;
  } catch (err) {
    console.log(err);
    return false;
  }
};

module.exports = {
  getWidgets
};