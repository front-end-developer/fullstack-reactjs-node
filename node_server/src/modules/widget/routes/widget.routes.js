"use strict";
/**
 * Created by Mark Webley on 05/07/2018.
 */
const Knex = require('../../../knex');
const Joi = require("joi");
const widgetsModel = require("../models/widgets.queries");

function getWidgets() {
  return {
    method: "GET",
    path: "/widgets/{uuid}",
    handler: async function (request, h) {
      let widgets = await widgetsModel.getWidgets(request.params.uuid);
      if (widgets.length === 1) {
        return h.response({
          result: "success",
          payload: widgets
        });
      } else {
        return h
          .response({
            result: "error",
            message: "no widgets exist"
          });
          //.code(404);
      }
    }
  };
}

module.exports = [
  getWidgets()
];