'use strict';

const bcrypt = require('bcrypt');
const Boom = require('boom');
const joi = require('joi');
const jwt = require('jsonwebtoken');
let Knex;

exports.plugin = {
    name: 'mark-webley-auth',
    version: '1.0.0',

    register: async function (server, options) {

        server.bind({
            Knex: server.app.knex
        });
        Knex = server.app.knex;
        await server.register(require('hapi-auth-bearer-token'));

        server.auth.strategy('simple', 'bearer-access-token', {
            allowQueryToken: true,
            validate: validateToken
        });

        // Create a route for example
        server.route({
            method: 'GET',
            path: '/test2',
            options: {
                auth: 'simple'
            },
            handler: function (request, h) {

                return 'hello, world';

            }
        });

        server.route({
            method: 'GET',
            path: '/test',
            handler: async function (request, h) {
                return "test";

            }
        });

        server.route({
            method: 'POST',
            path: '/login',
            config: {
                tags: ['api'],
                description: 'Return a JWT based on the provided credentials',
                //notes: '',
                validate: {
                    payload: {
                        user: joi.string().max(254),
                        password: joi.string().max(60)
                    }
                }
            },
            handler: async function (request, h) {

                const uuid = await validateUserPassword(request.payload.user, request.payload.password);
                console.log("uuid :" + uuid);
                if (uuid != '') {
                    try {
                        const jwtBearerToken = jwt.sign({
                            'roles': ['admin'],
                            'user': request.payload.user,
                            'uuid': uuid
                        }, 'MARK_WEBLEY_API_PRIVATE_KEY_1052', {
                            algorithm: 'HS256',
                            expiresIn: 64000,
                            subject: uuid
                        });
                        console.log(jwtBearerToken);
                        return {
                            token: jwtBearerToken
                        };
                    } catch (err) {
                        console.log(err);
                        return false;
                    }

                } else {
                    return Boom.unauthorized('WRONG_CREDENTIAL');
                }

            }
        });

    }
};

const validateToken = async (request, token) => {
    const credentials = {
        token
    };
    const artifacts = {
        test: 'info'
    };
    try {
        const isValid = jwt.verify(token, 'MARK_WEBLEY_API_PRIVATE_KEY_1052');
        return {
            isValid: true,
            credentials,
            artifacts
        };
    } catch (err) {
        return {
            isValid: false,
            credentials,
            artifacts
        };
    }
};

const validateUserPassword = async (login, password) => {
    try {
        const getOperation = await Knex('users')
            .select('uuid', 'password')
            .where({
                login: login
            });

        const res = await bcrypt.compare(password, getOperation[0].password);
        if (res) {
            return getOperation[0].uuid;
        } else {
            return "";
        }
    } catch (err) {
        console.log(err);
        return false;
    }
};