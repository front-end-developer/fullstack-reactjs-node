"use strict";
/**
 * Created by Mark Webley on 05/07/2018.
 * Used Hapi, Knex instead of express because it is faster to develope with, rapid development
 * I used a postgreSql database because I already had it running locally, I could use either mongodb, couchDB, mysql or postreSql or another. 
 */
const Hapi = require("hapi");
const Knex = require('./knex');
const appAuthorisation = require('./modules/authorisation/index');
const widgetRoutes = require('./modules/widget/routes/widget.routes');

const server = Hapi.server({
    port: 3000,
    host: "localhost",
    routes: {
        cors: {
            origin: ['*'],
            headers: ['Accept', 'Authorization', 'Content-Type', 'If-None-Match', 'Origin', 'X-Auth-Token'],
            additionalHeaders: ['cache-control', 'x-requested-with', 'Accept-language', 'Accept-Encoding', ]
        }
    }
});

const init = async () => {
    await server.register([
        require("inert"),
        require("vision"),
        {
            plugin: require("hapi-swaggered"),
            options: {
                info: {
                    title: "Mark Webley's backend API",
                    description: "Work in progress",
                    version: "0.1-dev"
                }
            }
        },
        {
            plugin: require("hapi-swaggered-ui"),
            options: {
                title: "Mark Webley's backend API",
                path: "/docs",
                authorization: {
                    field: "apiKey",
                    scope: "query", // header works as well
                    // valuePrefix: 'bearer '// prefix incase
                    defaultValue: "demoKey",
                    placeholder: "Enter your apiKey here"
                },
                swaggerOptions: {
                    validatorUrl: null
                }
            }
        }
    ]);

    server.app.knex = Knex;
    // await server.register(appAuthorisation);
    server.route(widgetRoutes);
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

process.on("unhandledRejection", err => {
    console.log(err);
    process.exit(1);
});

init();


