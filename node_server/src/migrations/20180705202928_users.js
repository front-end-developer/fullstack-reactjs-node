exports.up = function(knex, Promise) {
  return knex.schema
    .createTable("users", function (table) {
      table.uuid("uuid").notNullable().unique().primary();
      table.string("login", 50).notNullable();
      table.string("password", 255).notNullable();
      table.string("email", 255).notNullable();
      table.string('type', 100);
      table.string('active', 25 ).notNullable().defaultTo('active');
      table.string("createdBy", 255).notNullable();
      table.string("modifiedBy", 255).notNullable();
      table.timestamp("dateCreated").notNullable().defaultTo(knex.fn.now());
      table.timestamp("dateModified").notNullable().defaultTo(knex.fn.now());
    });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("users");
};
