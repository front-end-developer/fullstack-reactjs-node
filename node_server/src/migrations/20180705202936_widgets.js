
exports.up = function(knex, Promise) {
  return knex.schema
    .createTable("widgets", function (table) {
      table.uuid("uuid").notNullable().unique().primary();
      table.uuid("user_uuid").notNullable();
      table.json("widgets");
      table.string('active', 25 ).notNullable().defaultTo('active');
      table.string("createdBy", 254).notNullable();
      table.string("modifiedBy", 254).notNullable();
      table.timestamp("dateCreated").notNullable().defaultTo(knex.fn.now());
      table.timestamp("dateModified").notNullable().defaultTo(knex.fn.now());
    });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("widgets");
};
