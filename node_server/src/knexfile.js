module.exports = {
  development: {
    client: "postgresql",
	connection: 'postgres://markwebleyadmin:appijuiceTest@localhost/appijuiceDev',
    // connection: 'postgres://markwebleyadmin:appijuiceTest@dev.domainname.com/appijuiceDev',
	seeds: {
      directory: './db/seeds'
    }
  },

  staging: {
    client: "postgresql",
    connection: 'postgres://markwebleyadmin:appijuiceTest@localhost/appijuiceDev',
    // connection: 'postgres://markwebleyadmin:appijuiceTest@dev.domainname.com/appijuiceDev',
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  },

  test: {
    client: "postgresql",
    connection: 'postgres://markwebleyadmin:appijuiceTest@localhost/appijuiceDev',
    // connection: 'postgres://markwebleyadmin:appijuiceTest@dev.domainname.com/appijuiceDev',
    seeds: {
      directory: './db/test-seeds'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  },

  production: {
    client: "postgresql",
    connection: 'postgres://markwebleyadmin:appijuiceTest@localhost/appijuiceDev',
    // connection: 'postgres://markwebleyadmin:appijuiceTest@dev.domainname.com/appijuiceDev',
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  }
};