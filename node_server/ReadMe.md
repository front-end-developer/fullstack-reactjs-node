## How to run Node in Plesk:
* https://www.plesk.com/blog/product-technology/node-js-plesk-onyx/

## install postgreSQL
* https://www.pgadmin.org/download/
* go to: http://127.0.0.1:50383/browser/
* create database, users, passwords
* add the db, user, passwords to the files, knex.js & knexfile.js

## Setting up your local PG user:
* https://www.youtube.com/watch?v=nPbceHIPh4Y

## read up on knex migrations
* http://perkframework.com/v1/guides/database-migrations-knex.html
* knex migrate:make users
* knex migrate:make widgets