/**
 * Created by Mark Webley on 05/01/2018.
 */
import {resolve} from 'path';
import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import WebpackUglifyJsPlugin from 'webpack-uglify-js-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import 'babel-polyfill';

const webPackSettings = (env) => {
    const webpackConfig = {
        context: __dirname,
        entry: {
            vendors: ["jquery", "underscore", "font-awesome-sass-loader!./font-awesome-sass.config.js" /* , "popper" , "bootstrap" */],
            app: [
                'babel-polyfill',
                './src/app.js'
            ]
        },
        target: 'web',
        output: {
            crossOriginLoading: "anonymous",
            path: resolve(__dirname + '/build'),
            filename: 'bundle.[name].min.js',
            sourceMapFilename: '[name].map',
            publicPath: ''
        },
        devServer: {
            publicPath: resolve('/build/'),
            historyApiFallback: true
        },
        devtool: 'source-map',  // env.prod ? 'source-map' : 'eval', // 'cheap-module-source-map',
        resolve: {
            extensions: ['.js', '.jsx', '.ejs', '.json', '.html'],

            // Replace moduls with other modules or paths for compatiblility or convenience
            alias: {
                'React': 'react/addons',
                'jquery': 'jquery',
                'underscore': 'Underscore'//,
                //'popper': 'popper' //,
                //'bootstrap': 'bootstrap'
            }
        },

        stats: {
            colors: true,
            reasons: true,
            chunks: true
        },

        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.js$/,
                    loaders: ['babel-loader', 'eslint-loader'],
                    //loader: 'eslint-loader',
                    exclude: [/node_modules/, /lib/]
                },
                {
                    test: /\.ejs/,
                    use: [{
                        loader: 'ejs-loader'
                    }],
                    exclude: [/node_modules/, /lib/]
                },
                {
                    test: /\.ts$/,
                    loader: 'awesome-typescript-loader',
                    exclude: [/node_modules/, /lib/]
                },
                {
                    test: /\.json$/,
                    use: [{
                        loader: 'json-loader'
                    }],
                    exclude: [/node_modules/, /lib/]
                },
                {
                    test: /jquery\.js$/,
                    use: [{
                        loader: 'expose-loader',
                        options: '$'
                    }, {
                        loader: 'expose-loader',
                        options: 'jQuery'
                    }]
                }, {
                    test: /underscore/,
                    use: [{
                        loader: 'expose-loader',
                        options: '_'
                    }]
                }, {
                    test: /bootstrap/,
                    use: [{
                        loader: 'expose-loader',
                        options: 'bootstrap'
                    }]
                }, {
                    test: /\.js$/,
                    loaders: ['babel-loader'],
                    exclude: [/node_modules/, /lib/]
                }, {
                    test: /\.jsx$/,
                    enforce: "pre",
                    loaders: ['babel-loader'],
                    exclude: [/node_modules/, /lib/]
                }, {
                    test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 10000,
                                mimetype: 'application/font-woff'
                            }
                        }
                    ]
                }, {
                    // test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)(\?.*$|$)/,
                    use: [
                        { loader: 'file-loader' }
                    ]
                }, {
                    test: /\.scss$/,
                    use: [
                        "style-loader",
                        "css-loader",
                        "sass-loader"
                    ]
                },
                {
                    test: /\.css$/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    url: false
                                }
                            },
                            {
                                loader: 'autoprefixer-loader'
                            }
                        ]
                    })
                    //loaders: ['style-loader', 'css-loader', 'autoprefixer-loader']
                }
            ]
        },
        plugins: [
            new ExtractTextPlugin('styles.[name].min.css'),

            new WebpackUglifyJsPlugin({
                cacheFolder: resolve(__dirname, 'build/'),
                debug: true,
                minimize: false,
                sourceMap: true,
                output: {
                    comments: true
                },
                compressor: {
                    warnings: false,
                    keep_fnames: true
                },
                mangle: {
                    keep_fnames: true
                }
            })
        ]
    }

    console.log('NODE ENVIRONMENT: ', process.env.NODE_ENV);
    return webpackConfig;
}

module.exports = webPackSettings;