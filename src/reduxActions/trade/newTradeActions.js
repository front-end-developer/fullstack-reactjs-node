/**
 * Created by WebEntra on 28/06/2018.
 */
export const BUY = 'user:buy';
export const SELL = 'users:sell';

export function buy(trade) {
    return {
        type: BUY,
        payload: {
            user: trade
        }
    }
}

export function sell(trade) {
    return {
        type: SELL,
        payload: {
            user: trade
        }
    }
}