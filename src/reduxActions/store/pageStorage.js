/**
 * Created by WebEntra on 04/04/2018.
 */
export const pageStorage = (data) => {
    console.log('added data to pageStorage: ', JSON.stringify(data, null, 7) );
    return {
        type: 'INIT_PAGE_STORAGE',
        data: data
    }
}