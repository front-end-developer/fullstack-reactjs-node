/**
 * Created by Mark Webley on 01/04/2018.
 */
import React from 'react';
import { Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';

export default class Storage extends React.Component {
    constructor(props) {
        super(props);
        this.api = {
            getWidgets: 'http://localhost:3000/widgets/'
        };
        this.state = {
            storage: {
                widgets: []
            }
        }
    }

    getUserWidgets() {
        return Observable.fromPromise(fetch(`${this.api.getWidgets}ddb11fdc-29af-402e-8885-17f337201abd`).then(response => response.json()));
    }

    /*
    * TODO: set dynamically after login
    * temporarySolution for this test
    */
    loginAPISession(authKey) {
        localStorage.setItem('authKey', 'ddb11fdc-29af-402e-8885-17f337201abd');
    }
}