/**
 * Created by WebEntra on 04/05/2018.
 */
export class ContactFormModel {
    constructor(
        name,
        company,
        email,
        telephone,
        subject,
        message) {
        this.name = name;
        this.company = company;
        this.email = email;
        this.telephone = telephone;
        this.subject = subject;
        this.message = message;
    }
}
