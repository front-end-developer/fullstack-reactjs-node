/**
 * Created by Mark Webley on 06/07/2018.
 */
export class ContactFormModel {
    constructor(username, email, password) {
        this.email = email;
        this.username = username;
        this.password = password;
    }
}
