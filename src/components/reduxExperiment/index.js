import React from 'react';
import render from 'react-dom';
import { BrowserRouter  } from 'react-router-dom';
import './index.css';
import ReduxExperiment from './ReduxExperiment';
// import registerServiceWorker from './registerServiceWorker';

import {combineReducers, createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import productsReducer from './reducers/productsReducer';
import userReducer from './reducers/userReducer';
// import newTradeReducer from './reducers/newTradeReducer';

// to investigate more:
// thunk, promise, logger
/*
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import {createLogger} from 'redux-logger';
*/

// const logger = createLogger();
const allReducers = combineReducers({
    products: productsReducer,
    user: userReducer
})

const store = createStore(
    allReducers,
    {
        products: [{name: 'iPhone'}],
        user: 'Michael'
    },
    // applyMiddleware(thunk, promise, logger),
    window.devToolsExtension && window.devToolsExtension()
);

// console.log(store.getState());


render((
    <Provider store={store}>
        <BrowserRouter>
            <ReduxExperiment aRandomProps="whatever" />
        </BrowserRouter>
    </Provider>
), document.getElementById('root'));

// registerServiceWorker();
