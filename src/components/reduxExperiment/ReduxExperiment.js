/*
    TODO: fixed jest issues: https://stackoverflow.com/questions/39418555/syntaxerror-with-jest-and-react-and-importing-css-files
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './ReduxExperiment.scss';
import {updateUser} from '../../reduxActions/reduxExperiment/userActions';

import {bindActionCreators} from 'redux';
// import Router from './router/router';
// import Header from './components/common/header/index';

// connect your app to the store
import {connect} from 'react-redux';

class ReduxExperiment extends Component {
    constructor(props) {
        super(props);
        this.onUpdateUser = this.onUpdateUser.bind(this);
    }

    onUpdateUser(event) {
        this.props.onUpdateUser(event.target.value);
    }

    render() {
        console.log('render:', this.props);
        return (
            <div className="App">
                {
                    /*
                    <Header />
                    < Router / >
                    */
                }
                <p className="App-intro">
                    To get started, edit <code>src/App.js</code> and save to reload.
                </p>
                <input onChange={this.onUpdateUser}/>
                <br /> {this.props.user}
            </div>
        );
    }
}

ReduxExperiment.propTypes = {
    user: PropTypes.string,
    onUpdateUser: PropTypes.func
};

const mapStateToProps = (state, props) => {
    console.log('mapStateToProps:', props);

    return {
        products: state.products,
        user: state.user,
        userPlusProps: `${state.user} ${props.aRandomProps}`
    }
};

const mapActionsToProps = (dispatch, props) => {
    console.log('mapActionsToProps', props);

    return bindActionCreators({
        onUpdateUser: updateUser
    }, dispatch);
};

/*
 const mergeProps = (propsFromState, propsFromDispatch, ownProps) => {
 console.log('mergeProps: ', propsFromState, propsFromDispatch, ownProps);
 return {}
 }
 */

export default connect(mapStateToProps, mapActionsToProps /*, mergeProps */)(ReduxExperiment);
