import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Trade from './container/trade';
import TradeDetail from './detail/tradeDetail';

const TradeRoute = () => (
    <Switch>
        <Route exact path='/trade' component={Trade}/>
        <Route exact path='/trade/:number' component={TradeDetail}/>
    </Switch>
)

export default TradeRoute;