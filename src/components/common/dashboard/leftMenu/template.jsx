/**
 * Created by Mark Webley on 12/07/2018.
 */
import React from 'react'
import { Link } from 'react-router-dom'

export default (data) => {
    const context = data.context
    return (
      <section className="col left-nav">
          <div className="navbar navbar-default">
              <div className="nav-row-section">Screens</div>
              <div className="nav-row-section nav-add-button"><a className="btn btn-primary" href="/">+ Add</a></div>
              <nav className="navbar fixed-left">
                  <ul className="navbar-nav mr-auto">
                      <li className="nav-item"><Link className="nav-link" to='/'>screen name</Link></li>
                      <li className="nav-item"><Link className="nav-link" to='/'>screen name</Link></li>
                      <li className="nav-item"><Link className="nav-link" to='/'>screen name</Link></li>
                      <li className="nav-item"><Link className="nav-link" to='/'>screen name</Link></li>
                      <li className="nav-item"><Link className="nav-link" to='/'>screen name</Link></li>
                      <li className="nav-item"><Link className="nav-link" to='/'>screen name</Link></li>
                      <li className="nav-item"><Link className="nav-link" to='/'>screen name</Link></li>
                  </ul>
              </nav>
          </div>
      </section>
    )
}