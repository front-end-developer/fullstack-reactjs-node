/**
 * Created by Mark Webley on 29/06/2018.
 */
import React from 'react';
import {Switch, Route, Link} from 'react-router-dom';
import Storage from '../../../services/api/storage';
import './main-style.css';
import Template from './template';
import LeftMenuTemplate from './leftMenu/template';
import MainContentTemplate from './mainContent/template';
import RightMenuTemplate from './rightMenu/template';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.loading = false;
        this.onDragStart = this.onDragStart.bind(this);
        this.onDragOver = this.onDragOver.bind(this);
        this.onDrop = this.onDrop.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.state = {
            search: {
                keyword: ''
            },
            tasks: []
        }
    }
	
	/**
	* load services with fetch with polyfil or use RXJS
	*/
	componentDidMount() {
	    this.loading = true;
      let observer = {
          next: x => {
              this.loading = false;
              if (x.payload.length) {
                  this.setState({
                      'search': x.payload[0].widgets.search,
                      'tasks': x.payload[0].widgets.tasks
                  });
              }
          },
          error: err => console.error('Observer got an error: ' + err),
          complete: () => console.log('Observer got a complete notification')
      };
      new Storage().getUserWidgets().subscribe(observer);
	}

	onDragStart(e, id) {
	    console.log('onDragStart-id', id);
	    e.dataTransfer.setData('text', id);
	}
	
	/**
	* Place into its own component
	* elastic search with bounce
	*/
	onSearch(e) {
    console.log('onSearch name: ', e.target.name, ' value:', e.target.value);
		this.setState({
			search: {
				keyword: e.target.value
			}
		});
  }

  onDragOver(e) {
     e.preventDefault();
  }

	/**
	* TODO: set the put order, to the items are shown in order of how they are put
	*/
    onDrop(e, category) {
        e.preventDefault();
        let id = e.dataTransfer.getData('text');
        console.log('onDrop-tasks:', id, ' --onDrop:', category);
        let tasks = this.state.tasks.filter((task) => {
            if (task.name === id) {
                task.category = category;
            }
            return task;
        });
        console.log('onDrop-tasks:', JSON.stringify(tasks, null, 7));
        this.setState({
            ...this.state,
            tasks
        });
    }

    render() {
        var tasks = {
            widgetInventory: [],
            dashboard: []
        }

        this.state.tasks.forEach((t) => {
            console.log('name:', t.name);
            tasks[t.category].push(<div key={t.name} onDragStart={(e) => this.onDragStart(e, t.name)} draggable className="draggable" style={{backgroundColor: t.bgcolor}}>{t.name}</div>);
        });
		
        let widgetList = {
          widgetInventory: [],
                dashboard: []
        };
        let row = 1;
        this.state.tasks.map(item => {
          row++;
          let css = (row % 2 !== 0) ? 'col-5 widget float-left' : 'col-5 widget float-right';
          widgetList[item.category].push(<div key={item.name} className={css} onDragStart={(e) => this.onDragStart(e, item.name)} draggable>
            <figure>{css} {item.name}</figure>
            <figcaption>{
              // item.category
            }</figcaption>
          </div>);
        });
		
        return (
          <section className="container-fluid margin-top container-drag">
              <div className="row">
                  <LeftMenuTemplate context={this} />
                  <MainContentTemplate context={this} widgetList={widgetList} />
                  <RightMenuTemplate context={this} widgetList={widgetList} />
              </div>
          </section>
        )
    }
}

exports.Dashboard = Dashboard;