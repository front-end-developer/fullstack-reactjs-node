/**
 * Created by Mark Webley on 17/07/2018.
 */
describe('The dashboard', () => {

   beforeAll(() => {
       console.log('load service');
   });

    if('should return when done', done => {
        setTimeout(done, 100);
    });

   if('should do return a promise', () => {
        return new Promise(
          resolve => setTimeout(resolve, 100)
        );
   });

   if('should do async await', async () => await delay(100) );

   if('should render the main section', () => {
        expect(2 + 100).toEqual(102);
   });


});