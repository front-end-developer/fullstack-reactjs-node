/**
 * Created by Mark Webley on 12/07/2018.
 */
import React from 'react'
import { Link } from 'react-router-dom'

export default (data) => {
    const context = data.context;
    const widgetList = data.widgetList;
    return (
      <section className="col-7 main">
          <div className="btn-toolbar mw-toolbar tool-bar float-left" role="toolbar">
              <div className="btn-group btn-group-sm mr-2 mt-15" role="group" aria-label="First group">
                  <button type="button" className="btn btn-primary mr-2 float-right">Save</button>
                  <button type="button" className="btn btn-primary mr-2 float-right">Save new version</button>
                  <button type="button" className="btn btn-primary float-right">Submit for verification</button>
              </div>
          </div>
          <div className="droptarget"
               onDragOver={context.onDragOver}
               onDrop={(e) => context.onDrop(e, "dashboard")}>
              {widgetList.dashboard}
          </div>
      </section>

    )
}