/**
 * Created by Mark Webley on 12/07/2018.
 */
import React from 'react'
import { Link } from 'react-router-dom'

export default (data) => {
    const context = data.context;
    const widgetList = data.widgetList;
    return (
        <section className="col-3 right-nav widgetInventory" onDragOver={context.onDragOver} onDrop={(e) => { context.onDrop(e, "widgetInventory") }}>
          <div className="clearfix widget-search">
              <span className="float-left">Widgets</span>
              <input className="float-right" type="text" name="search" id="search" value={context.state.search.keyword} placeholder="search" onChange={context.onSearch} />
          </div>
          <h5>Charts</h5>
          {widgetList.widgetInventory}
      </section>
    )
}