import React from 'react'
import { Link } from 'react-router-dom'

export default (data) => {
    const context = data.context
    return (
        <header>
            <div className="navbar navbar-default">
                <nav className="navbar navbar-expand-md fixed-top nav-top">
                    <a className="navbar-brand" href="#">
                        <span className="logo"></span>
                        <span className="page">Dashboard</span>
                    </a>
                    <button onClick={context.toggleNavigation} className="navbar-toggler p-0 border-0" type="button"
                            data-toggle="offcanvas">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div onClick={context.toggleMenuButton} className="navbar-collapse collapse offcanvas-collapse">
                        <ul className="navbar-nav mr-auto profile-bar">
                            <li className="nav-item messages">
                                <Link className="nav-link" to='/'>
                                    <span className="fas fa-envelope"></span>
                                    <span className="notifications">2</span>
                                </Link>
                            </li>
                            <li className="nav-item alerts">
                                <Link className="nav-link" to='/about'>
                                    <span className="fas fa-bell"></span>
                                    <span className="notifications">7</span>
                                </Link>
                            </li>
                            <li className="nav-item avatar"><Link className="nav-link" to='/profile'><img src="/assets/uploads/my-profile-pic.jpg" /></Link></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
    )
}