/**
 * Created by WebEntra on 31/03/2018.
 */
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import dashboard from '../components/dashboard/container/index';
import TradeRoute from '../../trade/trade.routes';

const Router = () => (
    <main>
        <Switch>
            <Route exact path='/dashboard' component={dashboard}/>
            <Route path='/login' component={Login}/>
            <Route path='/trade' component={TradeRoute}/>
        </Switch>
    </main>
)
export default Router;