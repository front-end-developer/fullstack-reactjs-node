import {UPDATE_USER} from '../../reduxActions/reduxExperiment/userActions';

// function userReducer(state = '', action) { <-- use destructuring instead
export default function userReducer(state = '', {type, payload}) {
    let actionState;
    switch (type) {
        case UPDATE_USER:
            actionState = payload.user;
            break;
        default:
            actionState = state;
    }
    return actionState;
}
