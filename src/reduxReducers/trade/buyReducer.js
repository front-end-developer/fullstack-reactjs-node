import {BUY} from '../../actions/trade/newTradeActions';

export default function buyReducer(state = '', {type, payload}) {
    let actionState;
    switch (type) {
        case BUY:
            actionState = payload.user;
            break;
        default:
            actionState = state;
    }
    return actionState;
}
