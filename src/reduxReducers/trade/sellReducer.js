import {SELL} from '../../actions/trade/newTradeActions';

export default function sellReducer(state = '', {type, payload}) {
    let actionState;
    switch (type) {
        case SELL:
            actionState = payload.user;
            break;
        default:
            actionState = state;
    }
    return actionState;
}
