/**
 * Created by WebEntra on 04/04/2018.
 */
import {combineReducers} from 'redux';
import UserReducers from './reducer-users'
import ActivatedConsultancyService from './activatedConsultancyService';
import pageStorage from './pageStorage';
import productsReducer from './reduxExperiment/productsReducer';
import userReducer from './reduxExperiment/userReducer';
import newTradeReducer from './trade/newTradeReducer';

const allReducers = combineReducers({
    users: UserReducers,
    activatedConsultancyService: ActivatedConsultancyService,
    pageStorage: pageStorage,

    // NEW EXPERIMENTS
    products: productsReducer,
    user: userReducer
});
export default allReducers;
