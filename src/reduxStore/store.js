/**
 * Created by Mark Webley on 29/06/2018.
 */
import {createStore, applyMiddleware} from 'redux';
import allReducers from '../reduxReducers/allReducers';
// to investigate more:
// thunk, promise, logger
/*
 import thunk from 'redux-thunk';
 import promise from 'redux-promise';
 import {createLogger} from 'redux-logger';
 */


// const logger = createLogger();

const store = createStore(
    allReducers,
    {
        products: [{name: 'iPhone'}],
        user: 'Michael'
    },
    // applyMiddleware(thunk, promise, logger),
    window.devToolsExtension && window.devToolsExtension()
);