/**
 * Created by Mark Webley on 05/01/2018.
 */
import React from 'react';
import { render } from 'react-dom';
import { HashRouter, Switch, Route, Link, BrowserRouter } from 'react-router-dom';
// import {Provider} from 'react-redux';
// import {createStore} from 'redux';
import Header from './components/common/header/index';
import { Dashboard } from './components/common/dashboard/index';
import allReducers from './reduxReducers/allReducers';
// const store = createStore(allReducers);
import './app.scss';

const App = () => (
    <div>
        <Header />
        <Dashboard />
    </div>
)

render((
  //<Provider store={store}>
    <BrowserRouter>
        <App />
    </BrowserRouter>
 // </Provider>
), document.getElementById('app'));
