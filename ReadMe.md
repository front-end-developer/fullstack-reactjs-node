# Example of Front Application Architecture
* Author: Mark Webley
* Author mail: mrkwebley@gmail.com
* Author contact spanish mobile: uk whatsApp 0044 7930 743 923 | spanish mobile: 0034 631 33 61 33

## Time
Most of the time was spent on the application architecture, the architecture is the most important part, from there things run smoothly.


## To run front end do:
1. npm install
2. npm run build
3. npm run dev
4. localhost:8080

## The value of time, time is limited
This code is originally taken from my existing personal project, I am currently working on two projects at the sametime.


## Description
Only tested in Chrome, since this is a test. This code is an example of Front End Development, fullstack.

### Front End:
* custom application architecture,
* automated build,
* automated deployment,
* continuous integration,
* javascript ES6, ES7,
* React,
* webpack 3,
* npm,
* babel,
* eslint,
* sass,
* css3,
* html5,
* bootstrap 4,
* font awesome sass,
* redux,
* react,
* componants,
* services,
* fetch promises
* RxJS observers and observables

### Back End:
* javascript ES6
* async await
* node js,
* npm,
* bcrypt,
* knex,
* hapi,
* swagger,
* JWT (JSON Web Token),
* postgresql,
* pgAdmin

## To run do:
1. npm install -g knex
2. npm install knex pg --save

## Create the seed, cd to node_server/src, then run:
1. knex migrate:latest
2. knex seed:run

![Alt text](front-end-screens-2.png "Front End State in App Architecture")

![Alt text](front-end-screens-3.png "Front End State in App Architecture")

## Test the API with postman :)
![Alt text](backend-node-test-postman.png "Backend End State in App Architecture")


## Custom API: ##
GET: http://localhost:3000/widgets/{UUID}

API RESPONSE ON SUCCESS:
```javascript
    {
        "result": "success",
        "payload": [
            {
                "uuid": "ffb11fdc-29af-402e-8885-17f337201abd",
                "user_uuid": "ddb11fdc-29af-402e-8885-17f337201abd",
                "widgets": "\"{\"search\":{\"keyword\":\"\"},\"tasks\":[{\"name\":\"Angular\",\"category\":\"widgetInventory\",\"bgcolor\":\"yellow\"},{\"name\":\"React\",\"category\":\"widgetInventory\",\"bgcolor\":\"pink\"},{\"name\":\"Material\",\"category\":\"widgetInventory\",\"bgcolor\":\"yellow\"},{\"name\":\"redux\",\"category\":\"widgetInventory\",\"bgcolor\":\"pink\"},{\"name\":\"Vue\",\"category\":\"dashboard\",\"bgcolor\":\"skyblue\"},{\"name\":\"java\",\"category\":\"dashboard\",\"bgcolor\":\"skyblue\"}]}\"",
                "active": "active",
                "createdBy": "mark",
                "modifiedBy": "mark",
                "dateCreated": "2018-07-06T16:05:32.558Z",
                "dateModified": "2018-07-06T16:05:32.558Z"
            }
        ]
    }
```


